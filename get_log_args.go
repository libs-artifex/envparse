package envparse

import (
	"fmt"
	"log/slog"
	"os"
	"reflect"
)

// GetLogArgs - get arguments for slog.
func GetLogArgs(prefix string, obj any) ([]any, error) {
	args := make([]any, 0)

	value := reflect.ValueOf(obj)

	if value.Kind() != reflect.Ptr {
		return nil, ErrObjectIsNotPoint
	}

	value = value.Elem()
	typeOf := value.Type()

	if value.Kind() != reflect.Struct {
		return nil, ErrObjectIsNotStructure
	}

	if !value.CanSet() {
		return nil, ErrObjectIsNotInstallable
	}

loop:
	for i := 0; i < value.NumField(); i++ {
		field := typeOf.Field(i)

		tag, tagIfExists := field.Tag.Lookup(tagENV)
		if !tagIfExists {
			continue loop
		}

		if prefix != "" {
			tag = fmt.Sprintf("%s_%s", prefix, tag)
		}

		valueENV, valueIfExists := os.LookupEnv(tag)
		if !valueIfExists {
			ifRequired, ok := field.Tag.Lookup(tagRequired)
			if ok && ifRequired == "true" {
				return nil, newRequiredValueIsMissingError(tag)
			}

			defaultValue, ifDefault := field.Tag.Lookup(tagDefault)
			if ifDefault {
				args = append(args, slog.String(tag, defaultValue))

				continue loop
			}

			continue loop
		}

		args = append(args, slog.String(tag, valueENV))

		continue loop
	}

	return args, nil
}
