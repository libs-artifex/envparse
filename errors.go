package envparse

import (
	"errors"
	"fmt"
	"reflect"
)

var (
	ErrObjectIsNotInstallable = errors.New("the object is not installable")
	ErrObjectIsNotPoint       = errors.New("the object is not point")
	ErrObjectIsNotStructure   = errors.New("the object is not a structure")
	ErrReflectTypeInvalid     = errors.New("reflect type invalid")
)

type RequiredValueIsMissingError struct {
	value string
}

func (err RequiredValueIsMissingError) Error() string {
	return fmt.Sprintf("the required \"%s\" value is missing in the environment", err.value)
}

func newRequiredValueIsMissingError(value string) error {
	return RequiredValueIsMissingError{value: value}
}

type DataTypeDoesNotMatchTheExpectedError struct {
	dataType reflect.Kind
}

func (err DataTypeDoesNotMatchTheExpectedError) Error() string {
	return fmt.Sprintf("the type is not %s", err.dataType)
}

func newDataTypeDoesNotMatchTheExpectedError(dataType reflect.Kind) error {
	return DataTypeDoesNotMatchTheExpectedError{dataType: dataType}
}
