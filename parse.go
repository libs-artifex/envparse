package envparse

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"strconv"
)

const (
	tagENV      = "env"
	tagDefault  = "default"
	tagRequired = "required"
)

func Process(prefix string, obj any) error {
	value := reflect.ValueOf(obj)

	if value.Kind() != reflect.Ptr {
		return ErrObjectIsNotPoint
	}

	value = value.Elem()
	typeOf := value.Type()

	if value.Kind() != reflect.Struct {
		return ErrObjectIsNotStructure
	}

	if !value.CanSet() {
		return ErrObjectIsNotInstallable
	}

loop:
	for i := 0; i < value.NumField(); i++ {
		val := value.Field(i)
		field := typeOf.Field(i)

		tag, tagIfExists := field.Tag.Lookup(tagENV)
		if !tagIfExists {
			continue loop
		}

		if prefix != "" {
			tag = fmt.Sprintf("%s_%s", prefix, tag)
		}

		valueENV, valueIfExists := os.LookupEnv(tag)
		if !valueIfExists {
			ifRequired, ok := field.Tag.Lookup(tagRequired)
			if ok && ifRequired == "true" {
				return newRequiredValueIsMissingError(tag)
			}

			defaultValue, ifDefault := field.Tag.Lookup(tagDefault)
			if ifDefault {
				err := Set(val, defaultValue)
				if err != nil {
					return fmt.Errorf("error when writing the default value tag=%s value=%s error=%w", tag, valueENV, err)
				}

				continue loop
			}

			continue loop
		}

		err := Set(val, valueENV)
		if err != nil {
			return fmt.Errorf("error when writing data tag=%s value=%s error=%w", tag, valueENV, err)
		}

		continue loop
	}

	return nil
}

func Set(val reflect.Value, variable string) error {
	switch val.Kind() {
	case reflect.Invalid:
		return ErrReflectTypeInvalid
	case reflect.Bool:
		result, err := strconv.ParseBool(variable)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int)
		}

		val.SetBool(result)

		return nil
	case reflect.Int:
		result, err := strconv.ParseInt(variable, 10, 0)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int)
		}

		val.SetInt(result)

		return nil
	case reflect.Int8:
		result, err := strconv.ParseInt(variable, 10, 8)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int8)
		}

		val.SetInt(result)

		return nil
	case reflect.Int16:
		result, err := strconv.ParseInt(variable, 10, 16)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int16)
		}

		val.SetInt(result)

		return nil
	case reflect.Int32:
		result, err := strconv.ParseInt(variable, 10, 32)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int32)
		}

		val.SetInt(result)

		return nil
	case reflect.Int64:
		result, err := strconv.ParseInt(variable, 10, 64)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Int64)
		}

		val.SetInt(result)

		return nil
	case
		reflect.Uint:
		result, err := strconv.ParseUint(variable, 10, 0)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Uint)
		}

		val.SetUint(result)

		return nil
	case reflect.Uint8:
		result, err := strconv.ParseUint(variable, 10, 8)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Uint8)
		}

		val.SetUint(result)

		return nil
	case reflect.Uint16:
		result, err := strconv.ParseUint(variable, 10, 16)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Uint16)
		}

		val.SetUint(result)

		return nil
	case reflect.Uint32:
		result, err := strconv.ParseUint(variable, 10, 32)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Uint32)
		}

		val.SetUint(result)

		return nil
	case reflect.Uint64:
		result, err := strconv.ParseUint(variable, 10, 64)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Uint64)
		}

		val.SetUint(result)

		return nil
	case reflect.Uintptr:
		return ErrReflectTypeInvalid
	case reflect.Float32:
		result, err := strconv.ParseFloat(variable, 32)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Float32)
		}

		val.SetFloat(result)

		return nil
	case reflect.Float64:
		result, err := strconv.ParseFloat(variable, 64)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Float64)
		}

		val.SetFloat(result)

		return nil
	case reflect.Complex64:
		result, err := strconv.ParseComplex(variable, 64)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Complex64)
		}

		val.SetComplex(result)

		return nil
	case reflect.Complex128:
		result, err := strconv.ParseComplex(variable, 128)
		if err != nil {
			return newDataTypeDoesNotMatchTheExpectedError(reflect.Complex128)
		}

		val.SetComplex(result)

		return nil
	case reflect.Array:
		err := byteToReflect(val, variable)
		if err != nil {
			return fmt.Errorf("failed byte to array = %w", err)
		}

		return nil
	case reflect.Chan:
		return ErrReflectTypeInvalid
	case reflect.Func:
		return ErrReflectTypeInvalid
	case reflect.Interface:
		err := byteToReflect(val, variable)
		if err == nil {
			return nil
		}

		valueOf := reflect.ValueOf(variable)
		if valueOf.Kind() == reflect.String {
			val.Set(valueOf)

			return nil
		}

		return fmt.Errorf("failed byte to interface = %w", err)

	case reflect.Map:
		err := byteToReflect(val, variable)
		if err != nil {
			return fmt.Errorf("failed byte to map = %w", err)
		}

		return nil
	case reflect.Pointer:
		if val.IsNil() {
			return ErrReflectTypeInvalid
		}

		return Set(val.Elem(), variable)
	case reflect.Slice:
		err := byteToReflect(val, variable)
		if err != nil {
			return fmt.Errorf("failed byte to slice = %w", err)
		}

		return nil
	case reflect.String:
		val.SetString(variable)

		return nil
	case reflect.Struct:
		err := byteToReflect(val, variable)
		if err != nil {
			return fmt.Errorf("failed byte to struct = %w", err)
		}

		return nil
	case reflect.UnsafePointer:
		return ErrReflectTypeInvalid
	default:
		return ErrReflectTypeInvalid
	}
}

func byteToReflect(val reflect.Value, variable string) error {
	sl := reflect.New(val.Type()).Interface()

	err := json.Unmarshal([]byte(variable), &sl)
	if err != nil {
		return fmt.Errorf("fail json unmarshal: %w", err)
	}

	val.Set(reflect.ValueOf(sl).Elem())

	return nil
}
