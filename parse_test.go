package envparse_test

import (
	"errors"
	"maps"
	"os"
	"reflect"
	"testing"

	"gitlab.com/libs-artifex/envparse"
)

func TestProcess(t *testing.T) {
	t.Parallel()

	t.Run("happy path", func(t *testing.T) {
		err := os.Setenv("BOOL", "true")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("INT", "-1")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("INT8", "-8")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("INT16", "-16")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("INT32", "-32")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("INT64", "-64")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("UINT", "1")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("UINT8", "8")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("UINT16", "16")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("UINT32", "32")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("UINT64", "64")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("FLOAT32", "1.32")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("FLOAT64", "1.64")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("COMPLEX64", "-123.64")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("COMPLEX128", "-123.128")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("ARRAY_INT", "[1,2,3,4,5]")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("POINTER", "-123")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("SLICE_STRING", "[\"First\",\"Last\"]")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("SLICE_STRUCT", "[{\"data\":[5,4,3,2,1]},{\"data\":[1,2,3,4]}]")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("STRUCT", "{\"data\":[5,4,3,2,1]}")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		err = os.Setenv("STRING", "Hello world!")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		type testConfig struct {
			Bool        bool       `env:"BOOL"`
			Int         int        `env:"INT"`
			Int8        int8       `env:"INT8"`
			Int16       int16      `env:"INT16"`
			Int32       int32      `env:"INT32"`
			Int64       int64      `env:"INT64"`
			Uint        uint       `env:"UINT"`
			Uint8       uint8      `env:"UINT8"`
			Uint16      uint16     `env:"UINT16"`
			Uint32      uint32     `env:"UINT32"`
			Uint64      uint64     `env:"UINT64"`
			Float32     float32    `env:"FLOAT32"`
			Float64     float64    `env:"FLOAT64"`
			Complex64   complex64  `env:"COMPLEX64"`
			Complex128  complex128 `env:"COMPLEX128"`
			ArrayInt    [5]int     `env:"ARRAY_INT"`
			Pointer     *int       `env:"POINTER"`
			SliceString []string   `env:"SLICE_STRING"`
			SliceStruct []struct {
				Data []int `env:"data"`
			} `env:"SLICE_STRUCT"`
			Struct struct {
				Data []int `env:"data"`
			} `env:"STRUCT"`
			String string `env:"STRING"`
			Chan   chan any
			Func   func()
		}

		pointer := 321

		obj := testConfig{
			Pointer: &pointer,
		}

		err = envparse.Process("", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		resPointer := -123

		want := testConfig{
			Bool:        true,
			Int:         -1,
			Int8:        -8,
			Int16:       -16,
			Int32:       -32,
			Int64:       -64,
			Uint:        1,
			Uint8:       8,
			Uint16:      16,
			Uint32:      32,
			Uint64:      64,
			Float32:     1.32,
			Float64:     1.64,
			Complex64:   -123.64,
			Complex128:  -123.128,
			ArrayInt:    [5]int{1, 2, 3, 4, 5},
			Pointer:     &resPointer,
			SliceString: []string{"First", "Last"},
			SliceStruct: []struct {
				Data []int `env:"data"`
			}{
				{Data: []int{5, 4, 3, 2, 1}},
				{Data: []int{1, 2, 3, 4}},
			},
			Struct: struct {
				Data []int `env:"data"`
			}{Data: []int{5, 4, 3, 2, 1}},
			String: "Hello world!",
		}

		if !reflect.DeepEqual(want, obj) {
			t.Errorf("the object does not match the expected want=%v obj=%v", want, obj)

			return
		}
	})

	t.Run("INTERFACE_STRING", func(t *testing.T) {
		err := os.Setenv("INTERFACE", "Hello world!")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		type testConfig struct {
			Interface any `env:"INTERFACE"`
		}

		var obj testConfig

		err = envparse.Process("", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		want := testConfig{Interface: "Hello world!"}

		if !reflect.DeepEqual(want, obj) {
			t.Errorf("the object does not match the expected want=%v obj=%v", want, obj)

			return
		}
	})

	t.Run("INTERFACE_INT", func(t *testing.T) {
		err := os.Setenv("INTERFACE", "2")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		type testConfig struct {
			Interface any `env:"INTERFACE"`
		}

		var obj testConfig

		err = envparse.Process("", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		want := testConfig{Interface: float64(2)}

		if !reflect.DeepEqual(want, obj) {
			t.Errorf("the object does not match the expected want=%v obj=%v", want, obj)

			return
		}
	})

	t.Run("MAP", func(t *testing.T) {
		err := os.Setenv("MAP", "{\"Hello\":\"world\",\"Count\":\"2\"}")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		type testConfig struct {
			Map map[string]any `env:"MAP"`
		}

		var obj testConfig

		err = envparse.Process("", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		want := testConfig{Map: map[string]any{
			"Hello": "world",
			"Count": "2",
		}}

		if !maps.Equal(want.Map, obj.Map) {
			t.Errorf("the object does not match the expected want=%v obj=%v", want, obj)

			return
		}
	})

	t.Run("INVALID", func(t *testing.T) {
		type testConfig struct {
			Pointer *int `env:"INVALID"`
		}

		err := os.Setenv("INVALID", "-123")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		obj := testConfig{}

		err = envparse.Process("", &obj)
		if !errors.Is(err, envparse.ErrReflectTypeInvalid) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("CHAN", func(t *testing.T) {
		type testConfig struct {
			Chan chan any `env:"CHAN"`
		}

		err := os.Setenv("CHAN", "chan")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		obj := testConfig{}

		err = envparse.Process("", &obj)
		if !errors.Is(err, envparse.ErrReflectTypeInvalid) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("FUNC", func(t *testing.T) {
		type testConfig struct {
			Func func() any `env:"FUNC"`
		}

		err := os.Setenv("FUNC", "func")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		obj := testConfig{}

		err = envparse.Process("", &obj)
		if !errors.Is(err, envparse.ErrReflectTypeInvalid) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("REQUIRED", func(t *testing.T) {
		type testConfig struct {
			Bool bool `env:"IF_REQUIRED" required:"true"`
		}

		target := envparse.RequiredValueIsMissingError{}

		err := envparse.Process("", &testConfig{})
		if !errors.As(err, &target) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("REQUIRED", func(t *testing.T) {
		type testConfig struct {
			Default int `env:"IS_DEFAULT" default:"5"`
		}

		obj := testConfig{}

		err := envparse.Process("", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		if obj.Default != 5 {
			t.Errorf("еhe default value does not match the expected want=%v obj=%v", 5, obj.Default)

			return
		}
	})

	t.Run("IS_NOT_POINT", func(t *testing.T) {
		type testConfig struct{}

		obj := testConfig{}

		err := envparse.Process("", obj)
		if !errors.Is(err, envparse.ErrObjectIsNotPoint) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("IS_NOT_STRUCT", func(t *testing.T) {
		obj := true

		err := envparse.Process("", &obj)
		if !errors.Is(err, envparse.ErrObjectIsNotStructure) {
			t.Errorf("Process() error = %v", err)

			return
		}
	})

	t.Run("PREFIX", func(t *testing.T) {
		err := os.Setenv("PREFIX_BOOL", "true")
		if err != nil {
			t.Errorf("os.Setenv() error = %v", err)

			return
		}

		type testConfig struct {
			Bool bool `env:"BOOL"`
		}

		var obj testConfig

		err = envparse.Process("PREFIX", &obj)
		if err != nil {
			t.Errorf("Process() error = %v", err)

			return
		}

		if !obj.Bool {
			t.Errorf("the object does not match the expected want=%v obj=%v", true, obj.Bool)

			return
		}
	})
}
